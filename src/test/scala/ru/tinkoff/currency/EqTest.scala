package ru.tinkoff.currency

import org.scalatest.{FlatSpec, Matchers}
import ru.tinkoff.currency.Currency.{Rub, Usd}

class EqTest extends FlatSpec with Matchers {
  import ru.tinkoff.currency.Eq._
  import EqInstances._

  "Eq typeclass" should "return false for different currencies" in {
    val rub: Currency = Rub
    val usd: Currency = Usd

    (rub ==== usd) shouldBe false
  }

  it should "return true for equals currencies" in {
    val rub: Currency = Rub

    (rub ==== rub) shouldBe true
  }
}
