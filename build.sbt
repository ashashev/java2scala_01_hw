name := "java2scala-02"

version := "0.1"

scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.0.8" % Test,
  "com.beachape" %% "enumeratum" % "1.5.15",
  "org.rudogma" % "supertagged_2.13" % "1.5"
)

scalacOptions ++= Seq(
  "-Ywarn-dead-code",                  // Warn when dead code is identified.
  "-Ywarn-value-discard",              // Warn when non-Unit expression results are unused.

  "-unchecked",
  "-feature",
  "-deprecation:false",
  "-Xfatal-warnings",
)